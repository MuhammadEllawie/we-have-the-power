﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ChartManager : MonoBehaviour
{
    public float[] values;
    public Color[] wedgeColors;
    public Image wedgePrefab;
    public Image BG;
    public Image WheleBG;
    public Text ArTi;
    public Text EnTi;

    public Image[] pages;
    public Text[] percent;
    public Color fadedColor;
    List<Image> addedWdges = new List<Image>();
    public bool animating;
    // Use this for initialization
    void OnEnable()
    {
        for (int i = 0; i < 6; i++)
        {
            values[i] = PlayerPrefs.GetInt(i + "Yes");
        }
        BG.DOFade(0.9f, 0.5f);
        MakeGraph();
    }

    public void MakeGraph()
    {
        animating = true;
        float total = 0f;
        float zRotation = 0f;
        WheleBG.DOFade(0.0f, 0.5f);
        ArTi.DOFade(1, 0.25f);
        EnTi.DOFade(1, 0.25f);
        for (int i = 0; i < values.Length; i++)
        {
            total += values[i];
        }

        for (int i = 0; i < values.Length; i++)
        {
            Image newWedge = Instantiate(wedgePrefab) as Image;
            newWedge.transform.SetParent(transform, false);
            newWedge.color = wedgeColors[i];
            float delay = i;
            newWedge.DOFillAmount((values[i] / total), 0.5f).SetEase(Ease.OutCubic);//.SetDelay(0.5f * delay);
            newWedge.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, zRotation));/*DORotate(new Vector3(0.0f, 0.0f, zRotation), 0.5f)/*.SetEase(Ease.OutCubic)*/ //
            zRotation -= (values[i] / total) * 360f;
            percent[i].text = (int)((values[i] / total) * 100) + "%";
            addedWdges.Add(newWedge);
        }

        Invoke("DisplayPercents", 1.0f);
    }

    void DisplayPercents()
    {
        for (int i = 0; i < pages.Length; i++)
        {
            pages[i].DOFade(1f, 0.5f).SetDelay((i + 1) / 5.0f);
            percent[i].DOFade(1f, 0.5f).SetDelay((i + 1) / 5.0f);
        }
        animating = false;
    }

    public void CloseChartScene()
    {
        if (!animating)
        {
            WheleBG.DOFade(1.0f, 0.5f);
            ArTi.DOFade(0, 0.25f);
            EnTi.DOFade(0, 0.25f);
            for (int i = 0; i < 6; i++)
            {
                addedWdges[i].DOFade(0, 0.25f);
                pages[i].DOFade(0, 0.25f);
                percent[i].DOFade(0, 0.25f);
            }
            BG.DOFade(0.0f, 0.25f).SetDelay(0.2f).OnComplete(() =>
            {
                BG.gameObject.SetActive(false);
            });
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < addedWdges.Count; i++)
        {
            Destroy(addedWdges[i].gameObject);

        }
        
        for (int i = 0; i < pages.Length; i++)
        {
            pages[i].color = fadedColor;
            percent[i].color = fadedColor;
        }
        addedWdges.Clear();
    }


}
