﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using ArabicSupport;

public class GameManager : MonoBehaviour
{
    [Header("Variables")]
    public float transetionTime;
    public int currentPageNumber;
    public bool Animating;
    public bool[] AnswerdPages;
    public bool[] Answers;
    public float CoolTime;
    public bool GameTouched;
    public bool ScreenSaved;
    public int factCurrentPage;
    public float lastTouchedTime;
    Coroutine clrs;
    public bool restartNow;
    public bool moved;


    [Header("Page Referances")]
    public GameObject LockScreenObject;
    public Image Background;
    public Image LockBackground;
    public Transform factsArea;
    public Text PageTitle;
    public Text PageTitleEN;
    public Text CaptionAR;
    public Text CaptionEN;
    public Button StartButton;
    public Text StartButtonTxt;
    //public Text PowerTitle;
    //public Text PowerDiscription;
    //public Text PageComment;

    [Header("Voting System Referances")]
    public Text VoteText;
    public Image YesFiller;
    public Image NoFiller;
    public Image YesImg;
    public Image NoImg;
    public Image resultsBtnTxt;
    public Button resultsBtn;

    public Text thankfulText;
    public Image startOver;
    //public Image VoteButton;
    //public Image VoteInnerImage;

    [Header("Facts Referances")]
    public Text ArFact1;
    public Text ArFact2;
    public Text ArFact3;
    public Text ArFact4;
    public Text ArFact5;
    public Text ArFact6;

    [Space]

    public Text EnFact1;
    public Text EnFact2;
    public Text EnFact3;
    public Text EnFact4;
    public Text EnFact5;
    public Text EnFact6;

    public Button prevArr;
    public Button nextAr;

    public Text factsNum;

    [Header("Whiel Referances")]
    public Transform RotatedWhiel;
    public Image WhielImage;
    public Image WhielOuter;
    public Transform SolarButton;
    public Transform WindButton;
    public Transform OilButton;
    public Transform JiftButton;
    public Transform NuclearButton;
    public Transform HydroButton;
    public Image[] ButtonsBG;
    public Image[] WhielTitles;

    [Space]
    [Header("Page Data")]

    [Header("1: Solar | 2: Wind | 3: Oil | 4: Jift | 5: Nuclear | 6: Hydro")]

    public Color[] BackgroundColors;
    public Color[] PageTitleColors;
    public string[] Captions;
    public string[] PowersTitles;
    public string[] PowersDiscriptions;

    [Space]

    public string[] Facts1;
    public string[] Facts2;
    public string[] Facts3;
    public string[] Facts4;
    public string[] Facts5;
    public string[] Facts6;

    public string[] EnFacts1;
    public string[] EnFacts2;
    public string[] EnFacts3;
    public string[] EnFacts4;
    public string[] EnFacts5;
    public string[] EnFacts6;

    [Header("Whiel Data")]
    public float[] ZRotationStates;
    public Sprite[] WhielImages;

    [Space]

    public Sprite SolarButtonEmpty;
    public Sprite WindButtonEmpty;
    public Sprite OilButtonEmpty;
    public Sprite JiftButtonEmpty;
    public Sprite NuclearButtonEmpty;
    public Sprite HydroButtonEmpty;

    [Space]

    public Sprite SolarButtonFiller;
    public Sprite WindButtonFiller;
    public Sprite OilButtonFiller;
    public Sprite JiftButtonFiller;
    public Sprite NuclearButtonFiller;
    public Sprite HydroButtonFiller;


    private bool AnimFacts;
    Coroutine rotator;
    // Use this for initialization

    public void Start()
    {
        if (!PlayerPrefs.HasKey("delete"))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("All Prefs Deleted!");
            PlayerPrefs.SetInt("delete", 1);
        }
    }
    public void StartScene()
    {


        OpenPageNumber(0);

        PageTitle.DOFade(0.0f, 1f).SetDelay(0.2f);
        PageTitleEN.DOFade(0.0f, 1f).SetDelay(0.2f);
        CaptionAR.DOFade(0.0f, 1f).SetDelay(0.4f);
        CaptionEN.DOFade(0.0f, 1f).SetDelay(0.4f);
        StartButton.image.DOFade(0.0f, 1f).SetDelay(0.6f);
        StartButtonTxt.DOFade(0.0f, 1f).SetDelay(0.6f);
        LockBackground.DOFade(0.0f, 1f).SetDelay(0.8f).OnComplete(() =>
        {
            LockScreenObject.SetActive(false);
        });

        StartCoroutine(WaitForRestartTheScene());
        if (rotator == null)
        {
            rotator = StartCoroutine(RotateTheWheel());
        }

    }

    void ChangeBackGroundColor(int to)
    {
        Background.DOColor(BackgroundColors[to], transetionTime);
        for (int i = 0; i < ButtonsBG.Length; i++)
        {
            ButtonsBG[i].DOColor(BackgroundColors[to], transetionTime);
        }

    }

    void ChangeWheilImage(int imageIndex)
    {
        WhielImage.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            WhielImage.sprite = WhielImages[imageIndex];
            WhielImage.DOFade(1.0f, 2.0f).SetDelay(transetionTime - (transetionTime / 2));
        });

        WhielOuter.DOColor(new Color(PageTitleColors[imageIndex].r, PageTitleColors[imageIndex].g, PageTitleColors[imageIndex].b, 1), 2.0f);
    }

    void RotateWheilTo(int pageNumber)
    {
        Vector3 dest = new Vector3(0, 0, ZRotationStates[pageNumber]);
        RotatedWhiel.DORotate(dest, transetionTime).SetEase(Ease.OutCubic);

        Vector3 OppDest = new Vector3(0, 0, -ZRotationStates[pageNumber]);

        SolarButton.DOLocalRotate(OppDest, transetionTime).SetEase(Ease.OutCubic);
        WindButton.DOLocalRotate(OppDest, transetionTime).SetEase(Ease.OutCubic);
        OilButton.DOLocalRotate(OppDest, transetionTime).SetEase(Ease.OutCubic);
        JiftButton.DOLocalRotate(OppDest, transetionTime).SetEase(Ease.OutCubic);
        NuclearButton.DOLocalRotate(OppDest, transetionTime).SetEase(Ease.OutCubic);
        HydroButton.DOLocalRotate(OppDest, transetionTime).SetEase(Ease.OutCubic);


    }

    void ChangeTextsColor(int colorIndex)
    {
        //PageTitle.DOFade(0.0f, 0.25f).OnComplete(() =>
        //{
        //    PageTitle.color = PageTitleColors[colorIndex];
        //    PageTitle.DOFade(1.0f, 0.25f).OnComplete(() =>
        //    {

        //    }).SetDelay(transetionTime);
        //});

        //PageTitle.DOColor(Color.white, transetionTime);
        //ArFact1.color = PageTitleColors[colorIndex];
        //ArFact2.color = PageTitleColors[colorIndex];
        //ArFact3.color = PageTitleColors[colorIndex];
        //ArFact4.color = PageTitleColors[colorIndex];
        //ArFact5.color = PageTitleColors[colorIndex];
        //ArFact6.color = PageTitleColors[colorIndex];

        //EnFact1.color = PageTitleColors[colorIndex];
        //EnFact2.color = PageTitleColors[colorIndex];
        //EnFact3.color = PageTitleColors[colorIndex];
        //EnFact4.color = PageTitleColors[colorIndex];
        //EnFact5.color = PageTitleColors[colorIndex];
        //EnFact6.color = PageTitleColors[colorIndex];

        //PageComment.DOFade(0.0f, 0.25f).OnComplete(() =>
        //{
        //    PageComment.color = PageTitleColors[colorIndex];
        //    PageComment.DOFade(1.0f, 0.25f).OnComplete(() =>
        //    {

        //    }).SetDelay(transetionTime);
        //});
    }

    void ChangeTextsLevels(int textIndex)
    {
        factsArea.DOLocalMoveX(0, 0.1f, false).SetEase(Ease.OutCubic);
        factCurrentPage = 1;
        if (textIndex == 0)
        {
            factsNum.text = factCurrentPage + "/4";
        }
        else
        {
            factsNum.text = factCurrentPage + "/5";
        }
        nextAr.interactable = true;
        prevArr.interactable = false;
        ArFact1.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            ArFact1.text = Facts1[textIndex];//ArabicFixer.Fix(Facts1[textIndex]);
            ArFact1.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        ArFact2.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            ArFact2.text = Facts2[textIndex];//ArabicFixer.Fix(Facts2[textIndex]);
            ArFact2.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        ArFact3.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            ArFact3.text = Facts3[textIndex];// ArabicFixer.Fix(Facts3[textIndex]);
            ArFact3.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        ArFact4.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            ArFact4.text = Facts4[textIndex];// ArabicFixer.Fix(Facts4[textIndex]);
            ArFact4.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        ArFact5.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            ArFact5.text = Facts5[textIndex];// ArabicFixer.Fix(Facts4[textIndex]);
            ArFact5.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        ArFact6.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            ArFact6.text = Facts6[textIndex];// ArabicFixer.Fix(Facts4[textIndex]);
            ArFact6.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });


        EnFact1.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            EnFact1.text = EnFacts1[textIndex];//ArabicFixer.Fix(Facts1[textIndex]);
            EnFact1.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        EnFact2.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            EnFact2.text = EnFacts2[textIndex];//ArabicFixer.Fix(Facts2[textIndex]);
            EnFact2.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        EnFact3.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            EnFact3.text = EnFacts3[textIndex];// ArabicFixer.Fix(Facts3[textIndex]);
            EnFact3.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        EnFact4.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            EnFact4.text = EnFacts4[textIndex];// ArabicFixer.Fix(Facts4[textIndex]);
            EnFact4.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        EnFact5.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            EnFact5.text = EnFacts5[textIndex];// ArabicFixer.Fix(Facts4[textIndex]);
            EnFact5.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });
        EnFact6.DOFade(0.0f, 0.25f).OnComplete(() =>
        {
            EnFact6.text = EnFacts6[textIndex];// ArabicFixer.Fix(Facts4[textIndex]);
            EnFact6.DOFade(1.0f, 1.25f).SetDelay(transetionTime + 1.0f);
        });

    }

    void ChangeButtonsTitlesPos(int opendPage)
    {
        float fog = 1.00f;
        float t7t = -1.1f;
        switch (opendPage)
        {
            case 0:
                WhielTitles[0].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                  {
                      WhielTitles[0].rectTransform.DOAnchorPosY(fog, 0.1f);
                      WhielTitles[0].DOFade(1.0f, transetionTime * 0.5f);
                  });
                WhielTitles[1].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[1].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[1].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[2].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[2].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[2].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[3].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[3].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[3].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[4].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[4].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[4].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[5].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[5].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[5].DOFade(1.0f, transetionTime * 0.5f);
                });
                break;
            case 1:
                WhielTitles[0].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[0].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[0].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[1].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[1].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[1].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[2].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[2].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[2].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[3].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[3].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[3].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[4].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[4].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[4].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[5].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[5].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[5].DOFade(1.0f, transetionTime * 0.5f);
                });
                break;
            case 2:
                WhielTitles[0].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[0].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[0].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[1].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[1].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[1].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[2].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[2].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[2].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[3].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[3].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[3].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[4].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[4].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[4].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[5].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[5].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[5].DOFade(1.0f, transetionTime * 0.5f);
                });
                break;
            case 3:
                WhielTitles[0].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[0].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[0].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[1].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[1].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[1].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[2].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[2].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[2].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[3].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[3].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[3].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[4].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[4].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[4].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[5].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[5].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[5].DOFade(1.0f, transetionTime * 0.5f);
                });
                break;
            case 4:
                WhielTitles[0].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[0].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[0].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[1].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[1].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[1].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[2].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[2].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[2].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[3].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[3].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[3].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[4].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[4].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[4].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[5].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[5].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[5].DOFade(1.0f, transetionTime * 0.5f);
                });
                break;
            case 5:
                WhielTitles[0].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[0].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[0].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[1].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[1].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[1].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[2].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[2].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[2].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[3].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[3].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[3].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[4].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[4].rectTransform.DOAnchorPosY(t7t, 0.1f);
                    WhielTitles[4].DOFade(1.0f, transetionTime * 0.5f);
                });
                WhielTitles[5].DOFade(0.0f, transetionTime * 0.5f).OnComplete(() =>
                {
                    WhielTitles[5].rectTransform.DOAnchorPosY(fog, 0.1f);
                    WhielTitles[5].DOFade(1.0f, transetionTime * 0.5f);
                });
                break;
            default:
                break;
        }
    }

    public void FadeVotingSystem(float amount)
    {
        if (currentPageNumber != -1)
        {
            if (AnswerdPages[currentPageNumber] == false)
            {
                VoteText.DOFade(1, transetionTime);
            }
            else
            {
                VoteText.DOFade(0, transetionTime);
            }
        }
        YesFiller.DOFade(amount, transetionTime);
        NoFiller.DOFade(amount, transetionTime);
        YesImg.DOFade(amount, transetionTime);
        NoImg.DOFade(amount, transetionTime);

        //if (amount > 0)
        //{
        //    VoteButton.GetComponent<Button>().interactable = false;
        //    if (AnswerdPages[currentPageNumber] == false)
        //    {
        //        YesFiller.GetComponent<Button>().interactable = true;
        //        NoFiller.GetComponent<Button>().interactable = true;
        //    }

        //    VoteButton.DOFade(0, 0.25f);
        //    VoteInnerImage.DOFade(0, 0.25f);
        //    VoteButton.GetComponent<Button>().interactable = false;
        //    VoteButton.GetComponentInChildren<Text>().DOFade(0, 0.25f);//.SetDelay(transetionTime + 0.4f);

        //    ArFact1.DOFade(0, 0.25f);
        //    ArFact2.DOFade(0, 0.25f);
        //    ArFact3.DOFade(0, 0.25f);
        //    ArFact4.DOFade(0, 0.25f);
        //    ArFact5.DOFade(0, 0.25f);
        //    EnFact1.DOFade(0, 0.25f);
        //    EnFact2.DOFade(0, 0.25f);
        //    EnFact3.DOFade(0, 0.25f);
        //    EnFact4.DOFade(0, 0.25f);
        //    EnFact5.DOFade(0, 0.25f);

        //    nextAr.GetComponent<Image>().DOFade(0, 0.25f);
        //    prevArr.GetComponent<Image>().DOFade(0, 0.25f);
        //}
        //else
        //{
        //    nextAr.GetComponent<Image>().DOFade(1, 0.25f);
        //    prevArr.GetComponent<Image>().DOFade(1, 0.25f);

        //    YesFiller.GetComponent<Button>().interactable = false;
        //    NoFiller.GetComponent<Button>().interactable = false;


        //    VoteButton.GetComponent<Button>().interactable = true;

        //    thankfulText.DOFade(0, 0.25f);
        //    VoteText.DOFade(1.0f, 0.75f);
        //}
    }

    public void OpenPageNumber(int number)
    {
        if (true)//ScreenSaved == false)
        {
            if (!Animating)
            {
                //VoteButton.GetComponentInChildren<Text>().DOFade(0, 0.25f);
                if (number != currentPageNumber)
                {
                    SolarButton.GetComponent<Image>().sprite = SolarButtonEmpty;
                    WindButton.GetComponent<Image>().sprite = WindButtonEmpty;
                    OilButton.GetComponent<Image>().sprite = OilButtonEmpty;
                    JiftButton.GetComponent<Image>().sprite = JiftButtonEmpty;
                    NuclearButton.GetComponent<Image>().sprite = NuclearButtonEmpty;
                    HydroButton.GetComponent<Image>().sprite = HydroButtonEmpty;
                    switch (number)
                    {
                        case 0:
                            SolarButton.GetComponent<Image>().sprite = SolarButtonFiller;
                            break;
                        case 1:
                            WindButton.GetComponent<Image>().sprite = WindButtonFiller;
                            break;
                        case 2:
                            OilButton.GetComponent<Image>().sprite = OilButtonFiller;
                            break;
                        case 3:
                            JiftButton.GetComponent<Image>().sprite = JiftButtonFiller;
                            break;
                        case 4:
                            NuclearButton.GetComponent<Image>().sprite = NuclearButtonFiller;
                            break;
                        case 5:
                            HydroButton.GetComponent<Image>().sprite = HydroButtonFiller;
                            break;
                        default:
                            break;
                    }


                    Animating = true;
                    StartCoroutine(OpenPageSequence(number));
                }
            }
        }
    }

    IEnumerator OpenPageSequence(int page)
    {
        if (IsAnsweredAllPages() == false)
        {
            FadeVotingSystem(0.0f);
        }
        else
        {
            //yield return new WaitForSeconds(1.0f);
            FadeResultButton(0.0f);
        }
        thankfulText.DOFade(0, 0.75f);
        resultsBtnTxt.DOFade(0, 0.75f);
        //VoteText.DOFade(1.0f, 0.75f);

        prevArr.image.DOFade(0, 0.75f);
        nextAr.image.DOFade(0, 0.75f);
        factsNum.DOFade(0, 0.75f);

        //VoteButton.GetComponent<Button>().interactable = false;
        // VoteButton.DOFade(0, 0.25f);
        //VoteInnerImage.DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.1f);
        ChangeBackGroundColor(page);
        ChangeTextsColor(page);
        ChangeTextsLevels(page);
        ChangeWheilImage(page);
        RotateWheilTo(page);
        ChangeButtonsTitlesPos(page);
        currentPageNumber = page;
        ResetNewPage();

        yield return new WaitForSeconds(transetionTime + 1.6f);
        //VoteButton.color = new Color(BackgroundColors[page].r, BackgroundColors[page].g, BackgroundColors[page].b, 0.0f);
        if (IsAnsweredAllPages() == false)
        {
            FadeVotingSystem(1.0f);
        }
        else
        {
            FadeResultButton(1.0f);
        }

        prevArr.image.DOFade(1, 0.75f);
        nextAr.image.DOFade(1, 0.75f);
        factsNum.DOFade(1, 0.75f);

        //VoteButton.GetComponentInChildren<Text>().DOFade(1, 0.25f).SetDelay(transetionTime + 0.4f);
        //VoteInnerImage.DOFade(1, 1.25f).SetDelay(transetionTime + 0.4f);
        //VoteButton.DOFade(1, 1.25f).SetDelay(transetionTime + 0.4f).OnComplete(() =>
        //{
        //VoteButton.GetComponent<Button>().interactable = true;
        //});
        if (AnswerdPages[currentPageNumber] == false)
        {
            YesFiller.GetComponent<Button>().interactable = true;
            NoFiller.GetComponent<Button>().interactable = true;
        }
        else
        {
            thankfulText.DOFade(1, 0.75f);
        }
        Animating = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastTouchedTime = Time.timeSinceLevelLoad;
            GameTouched = true;
            if (rotator != null)
            {
                StopCoroutine(rotator);
            }
        }
    }

    void ResetNewPage()
    {
        if (AnswerdPages[currentPageNumber] == true)
        {
            YesFiller.GetComponent<Button>().interactable = false;
            NoFiller.GetComponent<Button>().interactable = false;
            UpdateFillingBars();
        }
        else
        {
            //YesFiller.GetComponent<Button>().interactable = true;
            //NoFiller.GetComponent<Button>().interactable = true;
            YesFiller.DOFillAmount(0.0f, 0.3f).SetEase(Ease.InCubic);
            NoFiller.DOFillAmount(0.0f, 0.3f).SetEase(Ease.InCubic);
        }
        if (!PlayerPrefs.HasKey(currentPageNumber + "Yes"))
        {
            PlayerPrefs.SetInt(currentPageNumber + "Yes", 0);
        }

        if (!PlayerPrefs.HasKey(currentPageNumber + "No"))
        {
            PlayerPrefs.SetInt(currentPageNumber + "No", 0);
        }
        //UpdateFillingBars();
    }

    public void VoteForYes()
    {
        if (true)//ScreenSaved == false)
        {
            YesFiller.DOFillAmount(1f, 1f);
            YesFiller.GetComponent<Button>().interactable = false;
            NoFiller.GetComponent<Button>().interactable = false;

            VoteText.DOFade(0.0f, 0.5f).OnComplete(() =>
            {
                thankfulText.DOFade(1, 0.5f);
                resultsBtnTxt.DOFade(1, 0.5f);
            });


            Debug.Log("Yes");
            Answers[currentPageNumber] = true;
            PlayerPrefs.SetInt(currentPageNumber + "Yes", PlayerPrefs.GetInt(currentPageNumber + "Yes") + 1);
            AnswerdPages[currentPageNumber] = true;
            UpdateFillingBars();
            bool ButtonAnimating = false;
            if (!ButtonAnimating)
            {
                ButtonAnimating = true;
                YesFiller.transform.DOScale(1.2f, 0.1f).OnComplete(() =>
                {
                    YesFiller.transform.DOScale(1.0f, 0.1f).OnComplete(() =>
                    {
                        ButtonAnimating = false;
                    });
                });
            }
        }
        if (IsAnsweredAllPages())
        {
            Invoke("SwitchVoteToResult", 3.0f);
        }
    }

    public void VoteForNo()
    {
        if (true)//ScreenSaved == false)
        {
            NoFiller.DOFillAmount(1f, 1f);
            YesFiller.GetComponent<Button>().interactable = false;
            NoFiller.GetComponent<Button>().interactable = false;


            VoteText.DOFade(0.0f, 0.5f).OnComplete(() =>
            {
                thankfulText.DOFade(1, 0.5f);
                resultsBtnTxt.DOFade(1, 0.5f);
            });


            Debug.Log("No");
            Answers[currentPageNumber] = false;
            PlayerPrefs.SetInt(currentPageNumber + "No", PlayerPrefs.GetInt(currentPageNumber + "No") + 1);
            AnswerdPages[currentPageNumber] = true;

            UpdateFillingBars();
            bool ButtonAnimating = false;
            if (!ButtonAnimating)
            {
                ButtonAnimating = true;
                NoFiller.transform.DOScale(1.2f, 0.1f).OnComplete(() =>
                {
                    NoFiller.transform.DOScale(1.0f, 0.1f).OnComplete(() =>
                    {
                        ButtonAnimating = false;
                    });
                });
            }
        }
        if (IsAnsweredAllPages())
        {
            Invoke("SwitchVoteToResult", 3.0f);
        }
    }

    void SwitchVoteToResult()
    {
        FadeVotingSystem(0);
        FadeResultButton(1);

    }

    void UpdateFillingBars()
    {
        float YesVots = PlayerPrefs.GetInt(currentPageNumber + "Yes");
        float NoVots = PlayerPrefs.GetInt(currentPageNumber + "No");
        float TotalVots = YesVots + NoVots;

        if (AnswerdPages[currentPageNumber] == true)
        {
            if (Answers[currentPageNumber])
            {
                YesFiller.DOFillAmount(1f, 1f);
                NoFiller.DOFillAmount(0f, 1f);
            }
            else
            {
                NoFiller.DOFillAmount(1f, 1f);
                YesFiller.DOFillAmount(0f, 1f);

            }
        }


        // YesFiller.DOFillAmount(1f, 1f);//((YesVots / TotalVots), 1.0f).SetEase(Ease.OutCubic);

        //NoFiller.DOFillAmount(1f, 1f);//((NoVots / TotalVots), 1.0f).SetEase(Ease.OutCubic);

        Debug.Log("Yes Vots: " + PlayerPrefs.GetInt(currentPageNumber + "Yes"));
        Debug.Log("No Vots: " + PlayerPrefs.GetInt(currentPageNumber + "No"));
    }

    bool IsAnsweredAllPages()
    {
        for (int i = 0; i < AnswerdPages.Length; i++)
        {
            if (AnswerdPages[i] == false)
            {
                return false;
            }
        }
        return true;
    }

    bool TimeToRestart()
    {

        if (Mathf.Abs(lastTouchedTime - Time.timeSinceLevelLoad) >= CoolTime)
        {
            return true;
        }
        return false;
    }

    bool TimeToRestart2()
    {

        if (Mathf.Abs(lastTouchedTime - Time.timeSinceLevelLoad) >= (CoolTime / 2))
        {
            return true;
        }
        return false;
    }

    public void RestartNow()
    {
        restartNow = true;
    }

    bool TimeEndOrPagesEnds()
    {
        if (TimeToRestart() || restartNow/*|| IsAnsweredAllPages()*/)
        {
            return true;

        }
        return false;

    }

    IEnumerator RotateTheWheel()
    {
        yield return new WaitUntil(TimeToRestart2);
        CoolTime += 30;
        OpenPageNumber(1);
        yield return new WaitForSeconds(4 + transetionTime);

        OpenPageNumber(2);
        yield return new WaitForSeconds(4 + transetionTime);

        OpenPageNumber(3);
        yield return new WaitForSeconds(4 + transetionTime);

        OpenPageNumber(4);
        yield return new WaitForSeconds(4 + transetionTime);

        OpenPageNumber(5);
        yield return new WaitForSeconds(4 + transetionTime);

        OpenPageNumber(0);
        yield return new WaitForSeconds(4 + transetionTime);

        RestartNow();
    }

    void FadeResultButton(float amount)
    {
        resultsBtn.interactable = true;
        resultsBtn.image.DOFade(amount, 0.5f);
        resultsBtnTxt.DOFade(amount, 0.5f);
        startOver.DOFade(amount, 0.5f);
        startOver.GetComponent<Button>().interactable = true;
        if (!moved)
        {
            thankfulText.DOFade(0f, 0.25f).OnComplete(() =>
            {
                thankfulText.text = "شكرا. تم الانتهاء من التصويت " + " \n" + "Voting is now complete  Thank you,";
                thankfulText.DOFade(1f, 0.25f);
            });
        }

        if (IsAnsweredAllPages())
        {
            if (!moved)
            {
                moved = true;
                resultsBtnTxt.DOFade(0, 0.5f).SetDelay(0.5f);
                startOver.DOFade(0, 0.5f).SetDelay(0.5f).OnComplete(() =>
                {

                    startOver.transform.localPosition = new Vector3(startOver.transform.localPosition.x, startOver.transform.localPosition.y + 150, startOver.transform.localPosition.z);
                    resultsBtnTxt.transform.localPosition = new Vector3(resultsBtnTxt.transform.localPosition.x, resultsBtnTxt.transform.localPosition.y + 150, resultsBtnTxt.transform.localPosition.z);


                    resultsBtnTxt.DOFade(1, 0.5f).SetDelay(0.5f);
                    startOver.DOFade(1, 0.5f).SetDelay(0.5f);
                });
            }
        }


    }

    IEnumerator WaitForRestartTheScene()
    {
        yield return new WaitUntil(TimeEndOrPagesEnds);

        if (TimeToRestart())
        {
            OpenPageNumber(1);
            yield return new WaitForSeconds(3 + transetionTime);
        }
        if (TimeToRestart())
        {
            OpenPageNumber(2);
            yield return new WaitForSeconds(3 + transetionTime);
        }
        if (TimeToRestart())
        {
            OpenPageNumber(3);
            yield return new WaitForSeconds(3 + transetionTime);
        }
        if (TimeToRestart())
        {
            OpenPageNumber(4);
            yield return new WaitForSeconds(3 + transetionTime);
        }
        if (TimeToRestart())
        {
            OpenPageNumber(5);
            yield return new WaitForSeconds(3 + transetionTime);
        }

        yield return new WaitUntil(TimeEndOrPagesEnds);
        //ScreenSaved = true;
        if (!GameTouched)
        {
            SolarButton.GetComponent<Button>().interactable = false;
            WindButton.GetComponent<Button>().interactable = false;
            OilButton.GetComponent<Button>().interactable = false;
            JiftButton.GetComponent<Button>().interactable = false;
            NuclearButton.GetComponent<Button>().interactable = false;
            HydroButton.GetComponent<Button>().interactable = false;

        }
        OpenPageNumber(0);
        yield return new WaitForSeconds(1.0f);
        LockBackground.DOFade(0.0f, 0.01f);
        LockScreenObject.SetActive(true);
        LockBackground.DOFade(1.0f, 1f).SetDelay(0.2f);

        yield return new WaitForSeconds(transetionTime * 2);
        SceneManager.LoadScene(0);

    }

    public void GoNextFact()
    {
        int numberOfPages = 0;
        if (currentPageNumber == 0)
        {
            numberOfPages = 5;
        }
        else
        {
            numberOfPages = 6;
        }
        if (!AnimFacts)
        {
            AnimFacts = true;
            if (factsArea.localPosition.x < -1599)
            {
                nextAr.interactable = false;
            }
            if (factsArea.localPosition.x > ((numberOfPages * -400) - 1))
            {
                prevArr.interactable = true;
                factsArea.DOLocalMoveX(factsArea.localPosition.x - 450, 0.5f, false).SetEase(Ease.OutCubic).OnComplete(() =>
                {
                    AnimFacts = false;
                });
                factCurrentPage++;
            }
            else
            {
                nextAr.interactable = false;
                AnimFacts = false;
            }
            if (factCurrentPage > 1)
            {
                factsNum.text = (factCurrentPage - 1) + "/" + (numberOfPages - 1);

            }
            else
            {
                factsNum.text = factCurrentPage + "/" + (numberOfPages - 1);
            }
            if (factCurrentPage == (numberOfPages))
            {
                nextAr.interactable = false;
            }
        }

    }

    public void GoPrevFact()
    {
        int numberOfPages = 0;
        if (currentPageNumber == 0)
        {
            numberOfPages = 5;
        }
        else
        {
            numberOfPages = 6;
        }
        if (!AnimFacts)
        {
            AnimFacts = true;
            if (factsArea.localPosition.x > -401)
            {
                prevArr.interactable = false;
            }
            if (factsArea.localPosition.x < 0)
            {
                nextAr.interactable = true;
                factsArea.DOLocalMoveX(factsArea.localPosition.x + 450, 0.5f, false).SetEase(Ease.OutCubic).OnComplete(() =>
                {
                    AnimFacts = false;
                });
                factCurrentPage--;
            }
            else
            {
                prevArr.interactable = false;
                AnimFacts = false;
            }
            if (factCurrentPage > 1)
            {
                factsNum.text = (factCurrentPage - 1) + "/" + (numberOfPages - 1);
            }
            else
            {
                factsNum.text = factCurrentPage + "/" + (numberOfPages - 1);
            }
        }

    }
}
